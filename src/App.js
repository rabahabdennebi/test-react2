import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import {sampleText} from './sampleText.js';

class App extends Component {
  state = {
    text : sampleText
  }
  hangleChange = event => {
    const text = event.target.value
    this.setState({text})
  }
  render() {
    return (
      <div className='container'>
        <div className='rows'>
          <div className='col-sm-6'>
            <textarea
              onChange={this.hangleChange}
              value={this.state.text}
              className='form-control'
              rows='35'/>
          </div>
          <div className='col-sm-6'>            
            <div>
              {sampleText}
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
